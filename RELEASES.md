# `1.0.0` - Carolina
Initial stable release of Tap Tracker.
There are still a few bugs and features that will be added in the near future,
but this version is safe to use and should be stable.
